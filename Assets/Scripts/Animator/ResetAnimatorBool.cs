﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAnimatorBool : StateMachineBehaviour
{
    public string[] BoolsToReset;
    public bool status;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        for (int i = 0; i < BoolsToReset.Length; i++)
        {
            animator.SetBool(BoolsToReset[i], status);
        }
    }
}
