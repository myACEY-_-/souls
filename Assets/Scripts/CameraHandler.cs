﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class CameraHandler : MonoBehaviour
    {
        Transform targetTransform;
        public Transform cameraTransform;
        public Transform cameraPivotTransform;
        private Transform myTransform;
        private Vector3 cameraTransformPos;
        public LayerMask ignoreLayers;
        private Vector3 cameraFollowVelocity = Vector3.zero;

        public static CameraHandler singelton;

        public float lookSpeed = 0.1f;
        public float followSpeed = 0.3f;
        public float pivotSpeed = 0.03f;

        private float defaultPos;
        private float lookAngle;
        private float pivotAngle;
        public float minPivot = -35;
        public float maxPivot = 35;

        private float targetPos;
        public float cameraSphereRadius = 0.2f;
        public float cameraCollisinOfset = 0.2f;
        public float minCollisionOffset = 0.2f;

        private void Awake()
        {
            singelton = this;
            myTransform = transform;
            defaultPos = cameraTransform.localPosition.z;
            ignoreLayers = ~(1 << 8 | 1 << 9 | 1 << 10);
            targetTransform = FindObjectOfType<PlayerManager>().transform;
        }

        public void FollowTarget(float delta)
        {
            Vector3 targetPos = Vector3.SmoothDamp(myTransform.position, targetTransform.position, ref cameraFollowVelocity, delta/ followSpeed);
            myTransform.position = targetPos;

            HandleCameraCollision(delta);

        }

        public void HandleCameraRotation(float delta, float mouseXInput, float mouseYInput)
        {
            lookAngle += (mouseXInput * lookSpeed) / delta;
            pivotAngle -= (mouseYInput * pivotSpeed) / delta;
            pivotAngle = Mathf.Clamp(pivotAngle, minPivot, maxPivot);

            Vector3 rotation = Vector3.zero;
            rotation.y = lookAngle;
            Quaternion targetRot = Quaternion.Euler(rotation);
            myTransform.rotation = targetRot;

            rotation = Vector3.zero;
            rotation.x = pivotAngle;

            targetRot = Quaternion.Euler(rotation);
            cameraPivotTransform.localRotation = targetRot;
        }

        private void HandleCameraCollision(float delta)
        {
            targetPos = defaultPos;
            RaycastHit hit;
            Vector3 dir = cameraTransform.position - cameraPivotTransform.position;
            dir.Normalize();

            if(Physics.SphereCast(cameraPivotTransform.position, cameraSphereRadius, dir, out hit, Mathf.Abs(targetPos), ignoreLayers))
            {
                float dis = Vector3.Distance(cameraPivotTransform.position, hit.point);
                targetPos = -(dis - cameraCollisinOfset);
            }

            if(Mathf.Abs(targetPos) < minCollisionOffset)
            {
                targetPos = -minCollisionOffset;
            }
            cameraTransformPos.z = Mathf.Lerp(cameraTransform.localPosition.z, targetPos, delta / 0.2f);
            cameraTransform.localPosition = cameraTransformPos;
        }
    }
}
