﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class EnemyStats : MonoBehaviour
    {
        public int healthLevel = 10;
        public int maxHealth;
        public int curHealth;

        //public HealthBar healthBar;

        Animator anim;


        void Start()
        {
            maxHealth = SetMaxHealthFromHealthLevel();
            curHealth = maxHealth;
            //healthBar.SetMaxHealth(maxHealth);
            anim = GetComponentInChildren<Animator>();
        }

        private int SetMaxHealthFromHealthLevel()
        {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }

        public void TakeDamage(int damage)
        {
            curHealth -= damage;
            anim.Play("TakeDamage_01");
            //healthBar.SetCurrentHealth(curHealth);

            if(curHealth <= 0)
            {
                curHealth = 0;
                anim.Play("Dead_01");
                //HANDLE ENEMY DEAD
            }
        }
    }
}