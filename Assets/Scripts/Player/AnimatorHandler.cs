﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class AnimatorHandler : MonoBehaviour
    {
        PlayerManager playerManager;

        public Animator anim;
        InputHandler inputHandler;
        PlayerLocomotion playerLocomotion;
        int vertical;
        int horizontal;
        public bool canRotate;

        public void Initialize()
        {
            playerManager = GetComponentInParent<PlayerManager>();
            anim = GetComponent<Animator>();
            inputHandler = GetComponentInParent<InputHandler>();
            vertical = Animator.StringToHash("vertical");
            horizontal = Animator.StringToHash("horizontal");
            playerLocomotion = GetComponentInParent<PlayerLocomotion>();
        }

        public void UpdateAnimValues(float verticalMovement, float horizontalMovemnt, bool isSprinting)
        {
            #region vertical
            float v = 0;

            if(verticalMovement > 0 && verticalMovement < 0.55f)
            {
                v = 0.5f;
            }
            else if(verticalMovement > 0.55f)
            {
                v = 1;
            }
            else if(verticalMovement < 0 && verticalMovement > -0.55f)
            {
                v = -0.5f;
            }
            else if(verticalMovement < -0.55f)
            {
                v = -1;
            }
            else
            {
                v = 0;
            }
            #endregion
            #region horizontal
            float h = 0;

            if (horizontalMovemnt > 0 && horizontalMovemnt < 0.55f)
            {
                h = 0.5f;
            }
            else if (horizontalMovemnt > 0.55f)
            {
                h = 1;
            }
            else if (horizontalMovemnt < 0 && horizontalMovemnt > -0.55f)
            {
                h = -0.5f;
            }
            else if (horizontalMovemnt < -0.55f)
            {
                h = - 1;
            }
            else
            {
                h = 0;
            }

            #endregion
            if (isSprinting)
            {
                v = 2;
                h = horizontalMovemnt;
            }

            anim.SetFloat(vertical, v, 0.1f, Time.deltaTime);
            anim.SetFloat(horizontal, h, 0.1f, Time.deltaTime);
        }
        
        public void PlayTargetAnim(string targetAnim, bool isInteract)
        {
            anim.applyRootMotion = isInteract;
            anim.SetBool("isInteract", isInteract);
            anim.CrossFade(targetAnim, 0.2f);
        }


        public void CanRotate()
        {
            canRotate = true;
        }
        public void StopRotation()
        {
            canRotate = false;
        }

        private void OnAnimatorMove()
        {
            if (playerManager.isInteract == false)
                return;

            float delta = Time.deltaTime;
            playerLocomotion.rb.drag = 0;
            Vector3 deltaPos = anim.deltaPosition;
            deltaPos.y = 0;
            Vector3 velocity = deltaPos / delta;
            playerLocomotion.rb.velocity = velocity;
        }

        public void EnableCombo()
        {
            anim.SetBool("canDoCombo", true);
        }

        public void DisableCombo()
        {
            anim.SetBool("canDoCombo", false);
        }
    }
}
