﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class PlayerAttacker : MonoBehaviour
    {
        AnimatorHandler animHandler;
        InputHandler inputHandler;
        WeaponSlotManager weaponSlotManager;
        public string lastAttack;

        private void Start()
        {
            animHandler = GetComponentInChildren<AnimatorHandler>();
            weaponSlotManager = GetComponentInChildren<WeaponSlotManager>();
            inputHandler = GetComponent<InputHandler>();
        }

        public void HandleWeaponCombo(WeaponItem weapon)
        {
            if(inputHandler.comboFlag)
            {
                animHandler.anim.SetBool("canDoCombo", false);
                if (lastAttack == weapon.OneHand_Light_Attack_1)
                {
                    animHandler.PlayTargetAnim(weapon.OneHand_Light_Attack_2, true);
                }
            }
        }

        public void HandleLightAttack(WeaponItem weapon)
        {
            weaponSlotManager.attackingWeapon = weapon;
            animHandler.PlayTargetAnim(weapon.OneHand_Light_Attack_1, true);
            lastAttack = weapon.OneHand_Light_Attack_1;
        }

        public void HandleHeavyAttack(WeaponItem weapon)
        {
            weaponSlotManager.attackingWeapon = weapon;
            animHandler.PlayTargetAnim(weapon.OneHand_Heavy_Attack_1, true);
            lastAttack = weapon.OneHand_Heavy_Attack_1;
        }
    }
}