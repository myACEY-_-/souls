﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class PlayerLocomotion : MonoBehaviour
    {
        PlayerManager playerManager;

        Transform cameraObject;
        InputHandler inputHandler;
        public Vector3 moveDir;

        [HideInInspector] public Transform myTransform;
        [HideInInspector] public AnimatorHandler animHandler;

        public Rigidbody rb;
        public GameObject normalCamera;

        [Header("Ground & Air Detection Stats")]
        [SerializeField] float grounndDetectionStartPoint = 0.5f; //beginRaycast
        [SerializeField] float minDistanceNeededToBeginFall = 1f; //dis to begin fall
        [SerializeField] float groundDirRayDistance = 0.2f; //offset
        LayerMask ignoreForGroundCheck;
        public float inAirTimer;

        [Header("Movement Stats")]
        [SerializeField] float movementSpeed = 5;
        [SerializeField] float walkingSpeed = 2;
        [SerializeField] float sprintSpeed = 7;
        [SerializeField] float rotationSpeed = 10;
        [SerializeField] float fallingSpeed = 45;
        
        void Start()
        {
            rb = GetComponent<Rigidbody>();
            inputHandler = GetComponent<InputHandler>();
            cameraObject = Camera.main.transform;
            myTransform = transform;
            animHandler = GetComponentInChildren<AnimatorHandler>();
            animHandler.Initialize();

            playerManager = GetComponent<PlayerManager>();

            playerManager.isGrounded = true;
            ignoreForGroundCheck = ~(1 << 8| 1<<11);

        }


        #region movement
        Vector3 normalVector;
        Vector3 targetPos;
        private void HandleRotation(float delta)
        {
            Vector3 targetDir = Vector3.zero;
            float moveDir = inputHandler.moveAmount;

            targetDir = cameraObject.forward * inputHandler.vertical;
            targetDir += cameraObject.right * inputHandler.horizntal;

            targetDir.Normalize();
            targetDir.y = 0;

            if (targetDir == Vector3.zero)
                targetDir = myTransform.forward;

            float rs = rotationSpeed;

            Quaternion tr = Quaternion.LookRotation(targetDir);
            Quaternion targetRot = Quaternion.Slerp(myTransform.rotation, tr, rs * delta);

            myTransform.rotation = targetRot;
        }

        public void HandleMovement(float delta)
        {
            if (inputHandler.rollFlag)
                return;

            if (playerManager.isInteract)
                return;

            moveDir = cameraObject.forward * inputHandler.vertical;
            moveDir += cameraObject.right * inputHandler.horizntal;
            moveDir.Normalize();
            moveDir.y = 0;

            float speed = movementSpeed;

            if(inputHandler.sprintFlag && inputHandler.moveAmount > 0.5f)
            {
                speed = sprintSpeed;
                playerManager.isSprinting = true;
                moveDir *= speed;
            }
            else
            {
                if(inputHandler.moveAmount < 0.5f)
                {
                    moveDir *= walkingSpeed;
                    playerManager.isSprinting = false;
                }
                else
                {
                    moveDir *= speed;
                    playerManager.isSprinting = false;
                }
            }

            Vector3 projectedVelocity = Vector3.ProjectOnPlane(moveDir, normalVector);
            rb.velocity = projectedVelocity;

            if (animHandler.canRotate)
            {
                HandleRotation(delta);
            }
            animHandler.UpdateAnimValues(inputHandler.moveAmount, 0, playerManager.isSprinting);
        }

        public void HandleRollingAndSprint(float delta)
        {
            if (animHandler.anim.GetBool("isInteract"))
                return;

            if (inputHandler.rollFlag)
            {
                moveDir = cameraObject.forward * inputHandler.vertical;
                moveDir += cameraObject.right * inputHandler.horizntal;

                if(inputHandler.moveAmount > 0)
                {
                    animHandler.PlayTargetAnim("Rolling", true);
                    moveDir.y = 0;
                    Quaternion rollRot = Quaternion.LookRotation(moveDir);
                    myTransform.rotation = rollRot;
                }
                else
                {
                    animHandler.PlayTargetAnim("Jump back", true);
                }
            }
        }

        public void HandleFalling(float delta, Vector3 moveDir)
        {
            playerManager.isGrounded = false;
            RaycastHit hit;
            Vector3 origin = myTransform.position;
            origin.y += grounndDetectionStartPoint;

            if(Physics.Raycast(origin, myTransform.forward, out hit, 0.4f))
            {
                moveDir = Vector3.zero;
            }

            if (playerManager.isInAir)
            {
                rb.AddForce(-Vector3.up * fallingSpeed);
                rb.AddForce(moveDir * fallingSpeed / 10f);
            }



            if (playerManager.isGrounded)
            {
                if (playerManager.isInteract || inputHandler.moveAmount > 0)
                {
                    myTransform.position = Vector3.Lerp(myTransform.position, targetPos, Time.deltaTime);
                }
                else
                {
                    myTransform.position = targetPos;
                }
            }

            Vector3 dir = moveDir;
            dir.Normalize();
            origin = origin + dir * groundDirRayDistance;

            targetPos = myTransform.position;

            Debug.DrawRay(origin, -Vector3.up * minDistanceNeededToBeginFall, Color.red, 0.1f, false);
            if(Physics.Raycast(origin, -Vector3.up, out hit, minDistanceNeededToBeginFall, ignoreForGroundCheck))
            {
                normalVector = hit.normal;
                Vector3 tp = hit.point;
                playerManager.isGrounded = true;

                if (playerManager.isInAir)
                {
                    if(inAirTimer > 0.5f)
                    {
                        Debug.Log("You were in the air for" + inAirTimer);
                        animHandler.PlayTargetAnim("Land", true);
                        inAirTimer = 0;
                    }
                    else
                    {
                        animHandler.PlayTargetAnim("Empty", false);
                        inAirTimer = 0;
                    }
                    playerManager.isInAir = false;
                }
            }
            else
            {
                if (playerManager.isGrounded)
                {
                    playerManager.isGrounded = false;
                }
                if(playerManager.isInAir == false)
                {
                    if(playerManager.isInteract == false)
                    {
                        animHandler.PlayTargetAnim("Falling", true);
                    }
                    Vector3 vel = rb.velocity;
                    vel.Normalize();
                    rb.velocity = vel * (movementSpeed / 2);
                    playerManager.isInAir = true;
                }
            }

            if (playerManager.isInteract || inputHandler.moveAmount > 0)
            {
                myTransform.position = Vector3.Lerp(myTransform.position, targetPos, Time.deltaTime / 0.1f);
            }
            else
                myTransform.position = targetPos;
        }

        #endregion
    }
}
