﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class PlayerManager : MonoBehaviour
    {
        InputHandler inputHandler;
        Animator anim;
        public bool isInteract;
        CameraHandler camHandler;
        PlayerLocomotion playerLocomotion;
        InteractableUI interactableUI;


        [Header ("Player Flags")]
        public bool isSprinting;
        public bool isInAir;
        public bool isGrounded;
        public bool canDoCombo;

        private void Awake()
        {
            camHandler = FindObjectOfType<CameraHandler>();
        }

        void Start()
        {
            inputHandler = GetComponent<InputHandler>();
            anim = GetComponentInChildren<Animator>();
            playerLocomotion = GetComponent<PlayerLocomotion>();
            interactableUI = FindObjectOfType<InteractableUI>();
        }

        void Update()
        {

            isInteract = anim.GetBool("isInteract");
            canDoCombo = anim.GetBool("canDoCombo");

            float delta = Time.deltaTime;
            inputHandler.TickInput(delta);
            playerLocomotion.HandleMovement(delta);
            playerLocomotion.HandleRollingAndSprint(delta);
            playerLocomotion.HandleFalling(delta, playerLocomotion.moveDir);
            CheckForInteractableObject();
        }
        private void FixedUpdate()
        {
            float delta = Time.deltaTime;

            if (camHandler != null)
            {
                camHandler.FollowTarget(delta);
                camHandler.HandleCameraRotation(delta, inputHandler.mouseX, inputHandler.mouseY);
            }
        }

        private void LateUpdate()
        {
            inputHandler.rollFlag = false;
            inputHandler.sprintFlag = false;
            inputHandler.rb_Input = false;
            inputHandler.rt_Input = false;
            inputHandler.d_Pad_Up = false;
            inputHandler.d_Pad_Down = false;
            inputHandler.d_Pad_Left = false;
            inputHandler.d_Pad_Right = false;
            inputHandler.a_Input = false;

            if (isInAir)
            {
                playerLocomotion.inAirTimer = playerLocomotion.inAirTimer + Time.deltaTime;
            }
        }

        public void CheckForInteractableObject()
        {
            RaycastHit hit;

            if (Physics.SphereCast(transform.position, 0.3f, transform.forward, out hit, 1))
            {
                print("+");
                if(hit.collider.tag == "Interactable")
                {
                    Interactable interactableObject = hit.collider.GetComponent<Interactable>();
                    if(interactableObject != null)
                    {
                        string interactableObjText = interactableObject.interactableText;
                        //Set UI text
                        interactableUI.interactableText.text = interactableObjText;
                        interactableUI.gameObject.SetActive(true);

                        if (inputHandler.a_Input)
                        {
                            hit.collider.GetComponent<Interactable>().Interact(this);
                        }
                    }
                }
            }
            else
            {
                if(interactableUI.gameObject != null)
                {
                    interactableUI.gameObject.SetActive(false);
                }
            }
        }
    }

    
}