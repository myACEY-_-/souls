﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class PlayerStats : MonoBehaviour
    {
        [Header("Health")]
        public int healthLevel = 10;
        public int maxHealth;
        public int curHealth;
        HealthBar healthBar;

        [Header("Stamina")]
        public int staminaLevel = 10;
        public int maxStamina;
        public int currentStamina;
        StaminaBar staminaBar;



        AnimatorHandler animHandler;


        void Start()
        {
            maxHealth = SetMaxHealthFromHealthLevel();
            curHealth = maxHealth;
            healthBar = FindObjectOfType<HealthBar>();
            healthBar.SetMaxHealth(maxHealth);
            animHandler = GetComponentInChildren<AnimatorHandler>();
            maxStamina = SetMaxStaminaFromStaminaLevel();
            currentStamina = maxStamina;
            staminaBar = FindObjectOfType<StaminaBar>();
            staminaBar.SetMaxStamina(maxStamina);
        }

        private int SetMaxHealthFromHealthLevel()
        {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }

        private int SetMaxStaminaFromStaminaLevel()
        {
            maxStamina = staminaLevel * 10;
            return maxHealth;
        }

        public void TakeDamage(int damage)
        {
            curHealth -= damage;
            animHandler.PlayTargetAnim("TakeDamage_01", true);
            healthBar.SetCurrentHealth(curHealth);

            if(curHealth <= 0)
            {
                curHealth = 0;
                animHandler.PlayTargetAnim("Dead_01", true);
                //HANDLE PLAYER DEAD
            }
        }

        public void TakeStaminaDamage(int damage)
        {
            currentStamina = currentStamina - damage;
            staminaBar.SetCurrentStamina(currentStamina);
        }
    }
}