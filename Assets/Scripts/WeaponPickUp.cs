﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG
{
    public class WeaponPickUp : Interactable
    {
        public WeaponItem weapon;

        public override void Interact(PlayerManager playerManager)
        {
            base.Interact(playerManager);

            //PickUpWeapon
            PickUpItem(playerManager);
        }

        private void PickUpItem(PlayerManager playerManager)
        {
            PlayerInventory playerInventory;
            PlayerLocomotion playerLocomotion;
            AnimatorHandler animHandler;

            playerInventory = playerManager.GetComponent<PlayerInventory>();
            playerLocomotion = playerManager.GetComponent<PlayerLocomotion>();
            animHandler = playerManager.GetComponentInChildren<AnimatorHandler>();

            playerLocomotion.rb.velocity = Vector3.zero; //Stops player
            animHandler.PlayTargetAnim("PickUpItem", true);//Play anim of pickUp
            playerInventory.weaponsInventory.Add(weapon);
            Destroy(gameObject);
        }
    }
}